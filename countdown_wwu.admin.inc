<?php

/**
* Implements hook_form().
*/
function countdown_wwu_form() {
   $form = array();

   $form['some_text'] = array(
      '#markup' => '<h3>At this point, input validation is not peformed. Please follow the date' .
      ' range format described below.</h3>'
   );

   # Note: for variable_get, the second parameter is a default_value
   $form['countdown_wwu_year'] = array(
      '#type' => 'textfield',
      '#title' => 'Year',
      '#description' => 'The target countdown year (e.g. 2020).',
      '#default_value' => variable_get('countdown_wwu_year', 2016),
      '#size' => 4,
      '#maxlength' => 4,
      '#required' => TRUE,
   );

   $form['countdown_wwu_month'] = array(
      '#type' => 'textfield',
      '#title' => 'Month',
      '#description' => 'The target countdown month (1 - 12).',
      '#default_value' => variable_get('countdown_wwu_month', 9),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
   );

   $form['countdown_wwu_day'] = array(
      '#type' => 'textfield',
      '#title' => 'Day',
      '#description' => 'The target countdown day (1 - 31).',
      '#default_value' => variable_get('countdown_wwu_day', 15),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
   );

   $form['countdown_wwu_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Header Text'),
      '#default_value' => variable_get('countdown_wwu_text', 'until fall 2020 move-in @ western'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => FALSE,
   );

   $form['files'] = array(
      '#type' => 'fieldset',
      '#title'=> t('Background Images'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
   );


   /* Create backround image forms */
   for ($i = 1; $i <= 6; $i++) {
      $img_name = 'wwu_countdown_background' . $i;
      $form[$img_name] = array(
         '#type' => 'managed_file',
         '#title' => 'Background Image ' . $i,
         '#description' => 'Click Browse... to upload an image.',
         '#default_value' => variable_get($img_name . '_fid', ''),
         '#upload_location' => 'public://',
         '#upload_validators' => array(
            'file_validate_extensions' => array('fig png jpg jpeg'),
         )
      );
   }

   $form['#submit'][] = 'countdown_wwu_form_submit';
   return system_settings_form($form);
}

/**
* Implements hook_form_validate().
*/
function countdown_wwu_form_validate($form, &$form_state) {
   if ($form == 'countdown_wwu_form') {
      $form_state['countdown_wwu_year'] = check_plain($form_state['countdown_wwu_year']);
      $form_state['countdown_wwu_month'] = check_plain($form_state['countdown_wwu_month']);
      $form_state['countdown_wwu_day'] = check_plain($form_state['countdown_wwu_day']);
   }
}

/**
 * Implements hook_form_submit()
 *
 */
function countdown_wwu_form_submit($form, &$form_state) {

   for ($i = 1; $i <= 6; $i++) {
      $img_name = 'wwu_countdown_background' . $i;
      $new_image = $form_state['values'][$img_name];
      $old_image = variable_get($img_name . '_fid');

      if (countdown_wwu_new_image_set($new_image)) {
         if (isset($old_image)) {
            if ($old_image != $new_image) {
               countdown_wwu_image_delete($old_image, $img_name);
               countdown_wwu_image_save($new_image, $img_name);
            }
         } else {
            countdown_wwu_image_save($new_image, $img_name);
         }
      } else {
         if (isset($old_image)) {
            countdown_wwu_image_delete($old_image, $img_name);
         }
      }
      unset($form_state['values'][$img_name]);
   }
}

function countdown_wwu_new_image_set($fid) {
   return is_numeric($fid) && !empty($fid);
}

function countdown_wwu_image_save($fid, $img_name) {
   $image = file_load($fid);
   if ($image) {
      $image->status = FILE_STATUS_PERMANENT;
      file_save($image);
      variable_set($img_name . '_fid', $image->fid);
      $block = block_load('countdown_wwu', 'wwu_admissions_block');
      file_usage_add($image, 'countdown_wwu', 'block', $block->bid);
      drupal_set_message(t('The uploaded image was saved successfully.'), 'status');
   } else {
      drupal_set_message(t('There was an error while saving the image. The image was not saved.'), 'error');
   }
}

function countdown_wwu_image_delete($fid, $img_name) {
   $image = file_load($fid);
   if ($image) {
      file_usage_delete($image, 'countdown_wwu');
      variable_del($img_name . '_fid');
      if (file_delete($image, TRUE)) {
         drupal_set_message(t('The previous image was deleted successfully.'), 'status');
      } else {
         drupal_set_message(t('There was an error while deleting the image. The image was not deleted.'), 'error');
      }
   } else {
      drupal_set_message(t('There was an error while delting the image. The image was not deleted.'), 'error');
   }
}