<div id="countdown_canvas">
	<div id="home">
		<a href="https://admissions.wwu.edu/">
			<img src="<?php echo $home_button; ?>">
		</a>
	</div>
	<div id="imgSelector">
		<form>
			<fieldset>
				<div id="countdown_radio">
					<input type="radio" id="countdown1"><label for="countdown1" class="selected__"></label>
					<input type="radio" id="countdown2"><label for="countdown2" class="unselected__"></label>
					<input type="radio" id="countdown3"><label for="countdown3" class="unselected__"></label>
					<input type="radio" id="countdown4"><label for="countdown4" class="unselected__"></label>
					<input type="radio" id="countdown5"><label for="countdown5" class="unselected__"></label>
					<input type="radio" id="countdown6"><label for="countdown6" class="unselected__"></label>
				</div>
			</fieldset>
		</form>
	</div>
	<div id="form">
	    <div id="topbox">
	        <div class="countdown"></div>
	        <div id="until">
				  <p><?php echo $header_text; ?></p>
			  </div>
	    </div>
	</div>
</div>